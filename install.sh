#!/bin/sh
echo "Installing aws studies app dependencies ........................."

go get github.com/go-pg/pg
go get github.com/go-pg/pg/orm
go get github.com/julienschmidt/httprouter

echo "Installed"