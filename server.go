package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/julienschmidt/httprouter"
)

var (
	DbUser = "heimdall"
	DbPd   = "heimdall17"
	DbDb   = "aws"
)

type Car struct {
	Id      int64
	Model   string
	Brand   string
	Variant string
	Color   string
}

func insertCar(car *Car) {
	db := pg.Connect(&pg.Options{
		User:     DbUser,
		Password: DbPd,
		Database: DbDb,
	})
	defer db.Close()

	err := db.Insert(car)
	if err != nil {
		panic(err)
	}
}

func findCar(car *Car) {
	db := pg.Connect(&pg.Options{
		User:     DbUser,
		Password: DbPd,
		Database: DbDb,
	})
	defer db.Close()

	err := db.Select(car)
	if err != nil {
		panic(err)
	}
}

func findAllCars() []Car {
	db := pg.Connect(&pg.Options{
		User:     DbUser,
		Password: DbPd,
		Database: DbDb,
	})

	var cars []Car
	err := db.Model(&cars).Select()
	if err != nil {
		panic(err)
	}

	return cars
}

func updateCar(car *Car) {
	db := pg.Connect(&pg.Options{
		User:     DbUser,
		Password: DbPd,
		Database: DbDb,
	})
	defer db.Close()

	err := db.Update(car)
	if err != nil {
		panic(err)
	}
}

func deleteCar(car *Car) {
	db := pg.Connect(&pg.Options{
		User:     DbUser,
		Password: DbPd,
		Database: DbDb,
	})
	defer db.Close()

	err := db.Delete(car)
	if err != nil {
		panic(err)
	}
}

func createSchema(db *pg.DB) error {
	for _, model := range []interface{}{(*Car)(nil)} {
		err := db.CreateTable(model, &orm.CreateTableOptions{
			Temp: false,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func FindAll(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	cars := findAllCars()

	a, err := json.Marshal(cars)

	if err != nil {
		log.Printf("Error reading body: %v", err)
	}

	w.Write(a)
}

func CreateTable(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	db := pg.Connect(&pg.Options{
		User:     DbUser,
		Password: DbPd,
		Database: DbDb,
	})
	defer db.Close()

	createSchema(db)
}

func Insert(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var c Car
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error reading body: %v", err)
		http.Error(w, "can't read body", http.StatusBadRequest)
		return
	}

	err2 := json.Unmarshal(body, &c)
	if err2 != nil {
		log.Fatal(err)
	}

	fmt.Printf(c.Brand)
	insertCar(&c)
}

func Update(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var c Car
	id := ps.ByName("id")
	numberID, err := strconv.ParseInt(id, 10, 64)

	if err != nil {
		log.Fatal(err)
	}

	body, err2 := ioutil.ReadAll(r.Body)
	if err2 != nil {
		log.Printf("Error reading body: %v", err)
		http.Error(w, "can't read body", http.StatusBadRequest)
		return
	}

	err3 := json.Unmarshal(body, &c)
	if err3 != nil {
		log.Fatal(err)
	}
	c.Id = numberID

	fmt.Printf(c.Brand)
	insertCar(&c)
}

func Delete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")

	numberID, err := strconv.ParseInt(id, 10, 64)

	if err != nil {
		log.Fatal(err)
	}

	car := Car{
		Id: numberID,
	}

	deleteCar(&car)
}

func main() {
	router := httprouter.New()
	router.GET("/car", FindAll)
	router.GET("/create-schema", CreateTable)
	router.PUT("/car/:id", Update)
	router.POST("/car", Insert)
	router.DELETE("/car/:id", Delete)

	log.Fatal(http.ListenAndServe(":8080", router))
}
